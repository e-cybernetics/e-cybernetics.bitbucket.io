function inputValidation(values) {
    let regular = /\s*,\s*/;
    return values.split(regular).filter(v => v >= 0);
}

function returnResult(result) {
    document.getElementById("result").innerHTML = result;
}

function check() {
    let input_array = inputValidation(document.getElementById("values").value);
    if (input_array.length < 2) {
        return returnResult('С введенных Вами данных нельзя получить масив натуральных чисел. Введите массив натуральных чисел в правильном формате.');
    }
    let result = doublerSearch(input_array);
    result = (result !== false) ? result : 'Дубликатов нет';
    return returnResult(result);
}

function doublerSearch(input_array) {
    //ищем середину массива
    let array_median = Math.floor((input_array.length - 1) / 2);
    //сравниваем средний элемент, и его ближайший соседний справа
    if (input_array[array_median] === input_array[array_median + 1]) {
        return input_array[array_median];
    }
    //смотрим можно ли массив еще делить
    if (input_array.length < 3) {
        return false;
    }
    //загоняем в рекурсию левый от середины массив
    let left_array_part_result = doublerSearch(input_array.slice(0, array_median + 1));
    //если не нашли в правом загоняем правый
    return left_array_part_result === false ? doublerSearch(input_array.slice(array_median + 1)) : left_array_part_result;
}